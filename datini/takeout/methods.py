import csv
import logging
from typing import BinaryIO, Generator
from zipfile import ZipFile

from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.db import transaction

from account.models import Clan
from expense.models import Expense
from takeout.fluent import bundle

_ = bundle.format_lazy

CONTENT_TYPES = [
    ("base", "user"),
    ("account", "clan"),
    ("expense", "recurringexpense"),
    ("expense", "expense"),
]
FIELDS = [
    ("who", _("expense-who")),
    ("who__email", _("takeout-field-email")),
    ("what", _("expense-what")),
    ("how_much", _("expense-how_much")),
    ("category", _("expense-category")),
    ("when", _("expense-when")),
    ("created_by", _("expense-created-by")),
]
BACKUP_FILE_FORMAT = "jsonl"
BACKUP_FILE_VERSION = 1
BACKUP_FILE_VERSION_PATH = "VERSION"
BACKUP_FILE_VERSION_CONTENT = f"version={BACKUP_FILE_VERSION}"
logger = logging.getLogger("datini.takeout")


class Echo:
    """An object that implements just the write method of the file-like
    interface.
    """

    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


def create_backup_file(handler: BinaryIO) -> BinaryIO:
    """ Create a backup file and write it to the handler file-like object.
    """
    serializer = serializers.get_serializer(BACKUP_FILE_FORMAT)()
    zipfile = ZipFile(handler, "w")
    zipfile.writestr(BACKUP_FILE_VERSION_PATH, BACKUP_FILE_VERSION_CONTENT)
    for app_label, model in CONTENT_TYPES:
        content_type = ContentType.objects.get(app_label=app_label, model=model)
        model_class = content_type.model_class()
        objects = model_class.objects.all()
        data = serializer.serialize(objects,
                                    use_natural_foreign_keys=True,
                                    use_natural_primary_keys=True)
        zipfile.writestr(f"{model}.{BACKUP_FILE_FORMAT}", data=data)
    zipfile.close()
    return handler


def restore_backup_file(path_or_object: BinaryIO, filename: str = None) -> None:
    """ Restore a backup file from the given file object.
        *Warning*: restoring a backup file is meant for a clean Datini instance.

        `path_or_object` should be a file-like object from the zipfile created by `create_backup_file`.

        The `filename` is used in logging to identify the backup file-object.
    """
    if filename is None:
        filename = id(path_or_object)
    zipfile = ZipFile(path_or_object, "r")
    with zipfile.open(BACKUP_FILE_VERSION_PATH, "r") as handler:
        version_string = handler.read()
        if not version_string != BACKUP_FILE_VERSION_CONTENT:
            raise ValueError("Template zip package error: wrong version. "
                             f"Expected {BACKUP_FILE_VERSION}, got {version_string}")

    with transaction.atomic():
        deserializer = serializers.get_deserializer(BACKUP_FILE_FORMAT)
        for app_label, model in CONTENT_TYPES:
            content_type = ContentType.objects.get(app_label=app_label, model=model)
            model_class = content_type.model_class()
            loaded_objects = []
            with zipfile.open(f"{model}.{BACKUP_FILE_FORMAT}", "r") as handler:
                for loaded in deserializer(handler):
                    loaded.object.pk = None
                    loaded_objects.append(loaded.object)
                model_class.objects.bulk_create(loaded_objects)
                logger.info("[%s][%s] Loaded %s object(s)", filename, model, len(loaded_objects))
    logger.info("[%s] Backup file restored", filename)


def get_csv(clan: Clan):
    """ This generator yields each line while composing a csv file (actually a *tab*-separated-values file) of
        all the expenses of the given clan
    """
    handler = Echo()
    writer = csv.writer(handler, dialect=csv.excel_tab)
    yield writer.writerow([c[1] for c in FIELDS])
    qs = Expense.all_objects.filter(clan=clan).order_by("-when")
    for row in qs.values_list(*[c[0] for c in FIELDS]):
        yield writer.writerow(row)
