from django_ftl.bundles import Bundle

bundle = Bundle(["base.ftl", "expense.ftl", "takeout.ftl"], auto_reload=False)
