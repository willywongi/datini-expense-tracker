from io import BytesIO

from django.http import StreamingHttpResponse, HttpResponse
from django.template.response import TemplateResponse
from django.utils import timezone

from base.views import clan_required
from .methods import create_backup_file, get_csv


@clan_required
def index(request):
    return TemplateResponse(request, template="takeout.html")


@clan_required
def takeout(request):
    filename = f"datini-takeout-{timezone.now():%Y%m%d%H%M%S}.csv"
    return StreamingHttpResponse(
        get_csv(clan=request.user.clan),
        content_type="text/csv",
        headers={"Content-Disposition": f'attachment; filename="{filename}"'},
    )


@clan_required
def backup(request):
    filename = f"datini-backup-{timezone.now():%Y%m%d%H%M%S}.zip"
    zip_handler = BytesIO()
    create_backup_file(zip_handler)
    return HttpResponse(zip_handler.getvalue(),
                        content_type="application/zip",
                        headers={"Content-Disposition": f'attachment; filename="{filename}"'})
