takeout-title = Data take out
takeout-help = For security, backup, analysis, portability. Your data is at your disposal. Download
    all your clan's expenses now.
takeout-field-email = Email
takeout-spreadsheet-action = Export
takeout-spreadsheet-subtitle = For spreadsheet or other software
takeout-spreadsheet-help = You will get a text file in TSV format with the date and time of the time
    of the export. You can also easily import this file into any spreadsheet program.
takeout-backup-action = Backup
takeout-backup-subtitle = For other Datini instances
takeout-backup-help = With the archive you get you can copy all your data to a new
    Datini instance. You will get a zip file containing files of type “jsonl.”
