takeout-title = Dati da asporto
takeout-help = Per sicurezza, backup, studio, portabilità. I tuoi dati sono a disposizione. Scarica
    tutte le spese del tuo clan ora.
takeout-field-email = Email
takeout-spreadsheet-action = Esporta
takeout-spreadsheet-subtitle = Per fogli di calcolo e altri software
takeout-spreadsheet-help = Otterrai un file di testo in formato TSV con la data e ora del momento
    dell'esportazione. Puoi facilmente importare questo file anche su qualsiasi programma di fogli
    di calcolo.
takeout-backup-action = Backup
takeout-backup-subtitle = Per altre istanze di Datini
takeout-backup-help = Con l'archivio che otterrai potrai copiare tutti i tuoi dati su una nuova
    istanza di Datini. Otterrai un file zip contenente dei file di tipo "jsonl".
