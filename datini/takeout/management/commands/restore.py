from pathlib import Path

from django.core.management.base import BaseCommand

from takeout.methods import restore_backup_file


class Command(BaseCommand):
    help = "Restore a backup file"

    def add_arguments(self, parser):
        parser.add_argument('path', type=Path, help="The path of the file to restore")

    def handle(self, *args, **options):
        with options["path"].open("rb") as handler:
            restore_backup_file(handler, filename=options["path"].name)