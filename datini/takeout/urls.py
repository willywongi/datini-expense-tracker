from django.urls import path

from . import views

app_name = "takeout"
urlpatterns = [
    path("", view=views.index, name="index"),
    path("takeout/", view=views.takeout, name="takeout"),
    path("backup/", view=views.backup, name="backup"),
]