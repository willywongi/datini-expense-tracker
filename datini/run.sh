#!/bin/sh

python manage.py collectstatic --noinput --ignore "bootstrap/*" --clear
python manage.py migrate --noinput

exec "$@"
