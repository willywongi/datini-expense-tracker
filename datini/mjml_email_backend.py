import mrml
from django.core.mail import EmailMessage
from django.core.mail.backends.smtp import EmailBackend


class MJMLEmailBackend(EmailBackend):
    def send_messages(self, email_messages: list[EmailMessage]) -> None:
        for message in email_messages:
            alternatives = getattr(message, "alternatives", None)
            if alternatives is not None:
                for i, (content, mime_type) in enumerate(alternatives):
                    content = content.strip()
                    if mime_type == "text/html" and content.startswith("<mjml>"):
                        message.alternatives[i] = mrml.to_html(content), mime_type
        return super().send_messages(email_messages)
