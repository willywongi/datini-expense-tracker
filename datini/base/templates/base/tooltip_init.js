document.addEventListener('readystatechange', () => {
  if (document.readyState === 'complete') {
    for (const element of document.querySelectorAll('[data-bs-toggle="tooltip"]')) {
      new bootstrap.Tooltip(element);
    }
  }
})
