import nanoid
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db.models import CharField, EmailField, ForeignKey, PROTECT, BigAutoField


def generate():
    return nanoid.generate('abcdefghijklmnopqrstuvwxyz0123456789', size=10)


class UserManager(BaseUserManager):
    """ To be used in conjunction with our custom User model that has
        the email field as the primary identifier.
    """

    def create_user(self, *args, **extra_field):
        """ Createsuperuser calls this method with (email, password, **extra_field).
            Socialauth pipeline calls this method with (email=email, password=password)
        """
        try:
            email, password = args
        except ValueError:
            email = extra_field.pop('email', None)
            password = extra_field.pop('password', None)

        if not email:
            raise ValueError("Email is required")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_field)
        if password:
            user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.update(is_staff=True, is_superuser=True, is_active=True)
        return self.create_user(email, password, **extra_fields)


class User(AbstractUser):
    """ Overwrites the base User model's id field using a varchar instead of a auto increment
        and using email as the primary unique key.
    """
    id = BigAutoField(primary_key=True)
    username = None
    email = EmailField("Email", unique=True)
    clan = ForeignKey("account.Clan", on_delete=PROTECT, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    @property
    def full_name(self):
        if self.first_name and self.last_name:
            return f"{self.first_name} {self.last_name}"
        else:
            return self.email.split("@")[0]

    @property
    def name(self):
        if self.first_name:
            return self.first_name
        else:
            return self.email.split("@")[0]

    def __str__(self):
        return self.full_name
