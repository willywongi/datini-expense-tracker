from django import template

register = template.Library()


@register.inclusion_tag("base/paginator_controls.html")
def paginator_controls(request, page_obj):
    has_previous = page_obj.has_previous()
    if has_previous:
        query = request.GET.copy()
        query['page'] = page_obj.previous_page_number()
        previous_url = f"?{query.urlencode()}"
    else:
        previous_url = None
    
    has_next = page_obj.has_next()
    if has_next:
        query = request.GET.copy()
        query['page'] = page_obj.next_page_number()
        next_url = f"?{query.urlencode()}"
    else:
        next_url = None
    
    return {
        'count': page_obj.paginator.count,
        'number': page_obj.number,
        'num_pages': page_obj.paginator.num_pages,
        'has_previous': page_obj.has_previous,
        'has_next': page_obj.has_next,
        'previous_url': previous_url,
        'next_url': next_url,
    }
