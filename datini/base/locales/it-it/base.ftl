-expense-set = {$first ->
   *[lowercase] spese
    [uppercase] Spese
}
expense = spesa

nav-expenses = {-expense-set(first:"uppercase")}
nav-app = App
nav-start-now = Inizia ora

hero-text = Un'altra app per le spese
what-is-datini =
    Datini è una web-app per il tracciamento delle spese, fatta da uno sviluppatore
    che non poteva accettare di usare Google Forms e Sheet.
character-alt-text = Il disegno stile cartoon di un contabile del diciottesimo secolo

clan-join = Entra in un clan
clan-join-help = Digita il codice del clan in cui vuoi entrare.
clan-join-action = Entra
clan-join-not-found-error = Nessun clan trovato con il codice %(code)s

clan-code = Codice clan
clan-title = Nome del clan

clan-create = Fai un nuovo clan
clan-create-help = Digita il nome del clan che vuoi creare.
clan-create-action = Crea

profile-clan = Fai parte del clan "{$clan}"
profile-title = Profilo
profile-update = Aggiorna il tuo profilo
profile-update-success = Il tuo profilo è stato aggiornato
profile-you = Tu

base-prev = Indietro
base-next = Avanti
base-pagination = Pagina {$number} di {$num_pages}.
base-update = Aggiorna
base-create = Crea
base-save = Registra
base-takeout = Dati da asporto
base-takeout-help = I dati sono tuoi. Portali a casa e usali!

time-today = Oggi
time-this-week = Questa settimana
time-this-month = Questo mese
time-other = Nel passato
