-expense-set = {$first ->
   *[lowercase] expenses
    [uppercase] Expenses
}
expense = expense

nav-expenses = {-expense-set(first:"uppercase")}
nav-app = App
nav-start-now = Start now

hero-text = Another expense tracking app
what-is-datini =
    Datini is a web app for tracking expenses, made by a single developer who grew tired of using
    Google Forms and Sheet.
character-alt-text = A cartoon style drawing of an 18th century accountant from Tuscany.

clan-join = Join a clan
clan-join-help = Enter the clan code you're going to join
clan-join-action = Join
clan-join-not-found-error = No clan found with code %(code)s

clan-code = Clan code
clan-title = Clan title

clan-create = Create a new clan
clan-create-help = Type the name of the clan you're creating
clan-create-action = Create

profile-clan = You belong to clan "{$clan}"
profile-title = Profile
profile-update = Update your profile
profile-update-success = Your profile has been updated succesfully.
profile-you = You

base-prev = Previous
base-next = Next
base-pagination = Page {$number} of {$num_pages}.
base-update = Update
base-create = Create
base-save = Save
base-takeout = Data takeout
base-takeout-help = Data is yours. Take it home and do something with it!

time-today = Today
time-this-week = This week
time-this-month = This month
time-other = In the past
