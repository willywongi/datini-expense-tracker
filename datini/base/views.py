from django.contrib.auth.decorators import user_passes_test
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView

font = TemplateView.as_view(template_name="base/font.css", content_type="text/css")
home = TemplateView.as_view(template_name="home/home.html")
clan_required = user_passes_test(lambda u: u.is_authenticated and u.clan is not None, login_url=reverse_lazy("account:clan"))
