from django_ftl.bundles import Bundle

bundle = Bundle(["base.ftl", "account.ftl"], auto_reload=False)
