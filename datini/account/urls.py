from django.contrib.auth.views import LogoutView
from django.urls import path

from . import views

app_name = "account"
urlpatterns = [
    # Getting into your account
    path("login", view=views.LoginView.as_view(), name="login"),
    path("logout", view=LogoutView.as_view(), name="logout"),
    path("reset_password", view=views.reset_password, name="reset_password"),
    path("reset_password/sent/", view=views.reset_password_sent, name='reset_password_sent'),
    path("reset_password/new/<uidb64>/<token>/", view=views.reset_password_new, name='reset_password_new'),
    path("reset_password/done/", view=views.reset_password_done, name='reset_password_done'),
    path("profile", view=views.profile, name="profile"),
    path("clan", view=views.clan, name="clan"),
    path("clan/join", view=views.clan_join, name="clan_join"),
    path("clan/create", view=views.clan_create, name="clan_create"),
    path("clan/update", view=views.clan_update, name="clan_update"),
]
