from django.contrib import messages
from django.contrib.auth import views, get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, \
    PasswordResetCompleteView
from django.shortcuts import render, redirect
from django.contrib.auth import forms
from django.urls import reverse_lazy
from django.views.decorators.http import require_http_methods

from account.forms import UserProfileForm, ClanForm, ClanJoinForm, ClanCreateForm
from account.models import Clan
from account.fluent import bundle
from base.views import clan_required

User = get_user_model()
_ = bundle.format_lazy


class LoginView(views.LoginView):
    template_name = 'account/login.html'


class UserCreationForm(forms.UserCreationForm):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name')


@clan_required
def profile(request):
    if request.method == "POST":
        form = UserProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, _("profile-update-success"))
            return redirect("account:profile")
    else:
        form = UserProfileForm(instance=request.user)

    context = {
        'profile_form': form,
        'clan_form': ClanForm(instance=request.user.clan),
        'user': request.user,
        'invitations': User.objects.filter(is_active=False, clan=request.user.clan),
        'clan_people': User.objects.filter(clan=request.user.clan),
        'title': _("account-profile")
    }

    return render(request, "account/profile.html", context=context)


@clan_required
@require_http_methods(["POST"])
def clan_update(request):
    form = ClanForm(request.POST, instance=request.user.clan)
    form.save()
    messages.add_message(request, messages.INFO, _("account-clan-update-success"))
    return redirect("account:profile")


@login_required
def clan(request):
    if request.user.clan is not None:
        return redirect("account:profile")

    context = {'join_form': ClanJoinForm(), 'create_form': ClanCreateForm()}
    return render(request, "account/clan.html", context=context)


@login_required
@require_http_methods(['POST'])
def clan_join(request):
    join_form = ClanJoinForm(request.POST)
    create_form = ClanCreateForm()
    if join_form.is_valid():
        clan = Clan.objects.get(code=join_form.cleaned_data['code'])
        request.user.clan = clan
        request.user.save()
        return redirect("account:profile")
    context = {'join_form': join_form, 'create_form': create_form}
    return render(request, "account/clan.html", context=context)


@login_required
@require_http_methods(['POST'])
def clan_create(request):
    join_form = ClanJoinForm()
    create_form = ClanCreateForm(request.POST)
    if create_form.is_valid():
        clan = Clan.objects.create(title=create_form.cleaned_data['title'])
        request.user.clan = clan
        request.user.save()
        return redirect("account:profile")
    context = {'join_form': join_form, 'create_form': create_form}
    return render(request, "account/clan.html", context=context)


# a post to this url send the link to reset their password
reset_password = PasswordResetView.as_view(success_url=reverse_lazy("account:reset_password_sent"),
                                           template_name="account/password_reset_form.html",
                                           email_template_name="account/password_reset_email.txt",
                                           html_email_template_name="account/password_reset_email.mjml",
                                           subject_template_name="account/password_reset_email_subject.txt")

# The page shown after a user has been emailed a link to reset their password.
reset_password_sent = PasswordResetDoneView.as_view(template_name="account/reset_password_sent.html")

# Presents a form for entering a new password.
reset_password_new = PasswordResetConfirmView.as_view(template_name="account/reset_password_new.html",
                                                      success_url=reverse_lazy("account:reset_password_done"))

# Presents a view which informs the user that the password has been successfully changed.
reset_password_done = PasswordResetCompleteView.as_view(template_name="account/reset_password_done.html")
