from django.contrib.auth import get_user_model, forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.forms import ModelForm, Form, CharField, EmailField, EmailInput

from account.models import Clan
from account.fluent import bundle

User = get_user_model()
_ = bundle.format_lazy


def clan_code_validator(value):
    try:
        Clan.objects.get(code=value)
    except Clan.DoesNotExist:
        raise ValidationError(
            _("clan-join-not-found-error"),
            params={"code": value},
        )


class UserProfileForm(ModelForm):
    class Meta:
        model = User
        fields = ("first_name", "last_name")


class ClanForm(ModelForm):
    class Meta:
        model = Clan
        fields = ("title", )
        labels = {"title": ""}


class ClanJoinForm(Form):
    code = CharField(max_length=6,
                     validators=[RegexValidator(regex=r"[ABCDEFGHLMNPQRSTUVWXYZ23456789]{6}"),
                                 clan_code_validator],
                     label=_("clan-code"),
                     help_text=_("clan-join-help"))


class ClanCreateForm(Form):
    title = CharField(max_length=128,
                      label=_("clan-title"),
                      help_text=_("clan-create-help"))

