account-profile = Profilo
account-login = Login
account-signup = Iscriviti
account-clan = Clan
account-clan-update-action = Aggiorna
account-clan-update-success = Clan aggiornato
account-logout = Disconnetti
account-logout-help = Logout: disconnetti questo dispositivo
account-logout-action = Logout
account-invite-title = Inviti
account-invite-create = Nuovo
account-invite-create-action = Crea e invia
account-invite-email = Indirizzo email
account-invite-help = Inserisci l'indirizzo email: verrà recapitato un invito per unirsi al tuo clan.
account-invite-success = Invito per { $email } spedito.
account-invite-already-invited = Questo indirizzo email è già stato invitato
account-invite-already-accepted = Questo indirizzo email ha già accettato un invito
account-invite-email-in-use = Un utente attivo sta usando questo indirizzo email
account-clan-people-title = Persone del tuo clan
account-clan-people-invite-create = Invita qualcuno
account-invite-link-not-valid-title = Ahi! Il link usato non è valido.
account-invite-link-not-valid-help = É probabile che sia scaduto oppure già usato. Se non hai ancora
    un account su Datini, chiedi a chi lo usa di mandarti un altro link di invito.
account-invite-send-again = Manda un altro link di invito
account-invite-set-password = Questo fa parte del tuo invito ad usare Datini! Scegli una password
    per il tuo nuovo account e sarà presto fatto.
account-invite-set-password-done-title = Hai fatto!
account-invite-set-password-done-help = Hai creato la password per il tuo account: usala ora per accedere
    a Datini.
account-password-reset-done-title = Fatto!
account-password-reset-done-help = Hai correttamente aggiornato la tua password. Puoi ora accedere a
    Datini.
account-password-reset-hint = Hai perso la password per accedere?
account-password-reset-start = Recupera la tua password
account-password-reset-form-help = Inserisci l'indirizzo email legato al tuo account. Se è corretto,
    riceverai un link valido per reimpostare la password.
account-password-reset-sent-title = Fatto
account-password-reset-sent-help = Se negli archivi di Datini c'è l'indirizzo email della richiesta,
    riceverai a brevissimo un link per reimpostare la password.
account-invite-message-preview = Questo è il tuo invito
account-invite-message-header = Unisciti al clan { $clan_title }
account-invite-message-body = Questo è il tuo invito per partecipare al clan { $clan_title } su Datini.
    Usa il link che segue per scegliere una password per il tuo nuovo account.
account-invite-message-button = Unisciti al clan
account-invite-message-signature = Inviato da { $sender }.
account-clan-people-invited = Utente invitato
account-clan-people-active = Utente attivo
account-invite-cancel = Annulla invito
account-invite-canceled = Invito annullato