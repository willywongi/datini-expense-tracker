account-profile = Profile
account-login = Login
account-signup = Signup
account-clan = Clan
account-clan-update-action = Update
account-clan-update-success = Clan updated
account-logout = Disconnect
account-logout-help = Logout: disconnect this device
account-logout-action = Logout
account-invite-title = Invites
account-invite-create = New
account-invite-create-action = Create and send
account-invite-email = Email address
account-invite-help = Write the email address: an invite to join your clan will be sent.
account-invite-success = Invite for { $email } sent.
account-invite-already-invited = This email address is already invited.
account-invite-already-accepted = This email address is tied to an existing user
account-invite-email-in-use = An active user is using this email address
account-clan-people-title = People in your clan
account-clan-people-invite-create = Send an invite
account-invite-link-not-valid-title = Ouch! This link is not working
account-invite-link-not-valid-help = This link could be timed out or already used. If you still don't
    have a Datini account, ask someone using the app to send you an invite.
account-invite-send-again = Send another invite link
account-invite-set-password = This is part of your invite for using Datini! Pick a password for your
    new account and you'll be ok in a zip.
account-invite-set-password-done-title = Zip! Done!
account-invite-set-password-done-help = You set a password for your account: use it now to log into
    Datini.
account-password-reset-done-title = Done!
account-password-reset-done-help = You set a new password for your account. You can now log into
    Datini.
account-password-reset-hint = Have you lost your password?
account-password-reset-start = Reset your password
account-password-reset-form-help = Write the email address of your account. If found on Datini, you
    will receive a link for setting a new password.
account-password-reset-sent-title = Done
account-password-reset-sent-help = If the requested email address is found on Datini, you'll briefly
    receive a message with a link to reset your password.
account-invite-message-preview = This is your invite
account-invite-message-header = Join clan { $clan_title }
account-invite-message-body = This is your invite to join cla { $clan_title } on Datini. Use the
    following link to set a password for your new account.
account-invite-message-button = Join the clan
account-invite-message-signature = Sent by { $sender }.
account-clan-people-invited = Invited
account-clan-people-active = Active
account-invite-cancel = Cancel invite
account-invite-canceled = Invite canceled
