# Generated by Django 4.2.1 on 2023-08-15 14:14

import account.models
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Clan',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default='', max_length=128)),
                ('code', models.CharField(default=account.models.generate_clan_code, max_length=6)),
            ],
        ),
        migrations.AddConstraint(
            model_name='clan',
            constraint=models.UniqueConstraint(fields=('code',), name='unique_by_code'),
        ),
    ]
