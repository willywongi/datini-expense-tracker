import nanoid
from django.conf import settings
from django.db.models import Model, CharField, UniqueConstraint, ManyToManyField


def generate_clan_code():
    return nanoid.generate("ABCDEFGHLMNPQRSTUVWXYZ23456789", size=6)


class Clan(Model):
    title = CharField(max_length=128, default="")
    code = CharField(max_length=6, default=generate_clan_code)

    class Meta:
        constraints = [UniqueConstraint(fields=("code", ), name="unique_by_code")]
