import datetime
import logging
from zoneinfo import ZoneInfo

from django.db import transaction
from django.utils import timezone
from django.utils.timezone import make_naive
import dramatiq

from expense.models import Expense, RecurringExpense

logger = logging.getLogger("datini.expense.recurring")


def create_expense_from_recurrence(recurring_expense, dtnow=None):
    with transaction.atomic():
        now = dtnow or timezone.now()
        what = recurring_expense.what.format(year=now.year, month=now.month, day=now.day)
        expense = Expense(created_by=recurring_expense.created_by,
                          clan=recurring_expense.clan,
                          who=recurring_expense.who,
                          what=what,
                          how_much=recurring_expense.how_much,
                          category=recurring_expense.category,
                          when=datetime.datetime.now(datetime.timezone.utc))
        expense.save()
        recurring_expense.next_occurrence = get_next_occurrence(recurring_expense.recurrences, recurring_expense.time, recurring_expense.time_zone)
        recurring_expense.save()
    logger.info("[%s] Created expense from recurring spec (%s)", recurring_expense.id, expense.what)


def get_next_occurrence(recurrences, time, time_zone):
    zone_info = ZoneInfo(time_zone)
    naive_localnow = make_naive(timezone.now(), timezone=zone_info)
    naive_next = recurrences.after(naive_localnow)
    return datetime.datetime.combine(naive_next.date(),
                                       time=time,
                                       tzinfo=ZoneInfo(time_zone))


@dramatiq.actor
def create_current_expenses(current_datetime=None):
    if not current_datetime:
        current_datetime = timezone.now().replace(minute=0, second=0, microsecond=0)
    recurring_expenses = RecurringExpense.objects.filter(next_occurrence=current_datetime)
    logger.info("[%s] Set to create %s expenses", current_datetime, recurring_expenses.count())
    for recurring_expense in recurring_expenses:
        create_expense_from_recurrence(recurring_expense, dtnow=current_datetime)