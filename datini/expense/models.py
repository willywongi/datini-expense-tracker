from datetime import time
import zoneinfo

from django.conf import settings
from django.db.models import deletion, Model, Manager, ForeignKey
from django.db.models.enums import TextChoices
from django.db.models.fields import related, DateTimeField, CharField, BooleanField, TimeField
from django.utils import timezone
from djmoney.models.fields import MoneyField
from recurrence.fields import RecurrenceField

from .fluent import bundle

_ = bundle.format_lazy


class ExpenseCategory(TextChoices):
    HOUSEHOLD = 'HOUSEHOLD', _("expense-category-household")
    GROCERIES = 'GROCERIES', _("expense-category-groceries")
    ENTERTAINMENT = 'ENTERTAINMENT', _("expense-category-entertainment")
    UTILITIES = 'UTILITIES', _("expense-category-utilities")
    PETS = 'PETS', _("expense-category-pets")
    FINANCE = 'FINANCE', _("expense-category-finance")
    KIDS = 'KIDS', _("expense-category-kids")
    

class ExpenseManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(deleted_at__isnull=True)
    

class Expense(Model):
    objects = ExpenseManager()
    all_objects = Manager()
    
    created_by = related.ForeignKey('base.User', null=True, on_delete=deletion.PROTECT, related_name="+")
    clan = related.ForeignKey('account.Clan', on_delete=deletion.PROTECT)
    who = related.ForeignKey('base.User', on_delete=deletion.PROTECT)
    what = CharField(max_length=255, default='', blank=True)
    how_much = MoneyField(max_digits=14, decimal_places=2, default=0)
    category = CharField(max_length=255, choices=ExpenseCategory.choices)
    when = DateTimeField(null=True)
    deleted_at = DateTimeField(default=None, null=True)
    recurring_expense = ForeignKey('RecurringExpense', on_delete=deletion.SET_NULL, null=True, blank=True)
    
    def soft_delete(self):
        self.deleted_at = timezone.now()
        self.save()
    
    def restore(self):
        self.deleted_at = None
        self.save()


class RecurringExpense(Model):
    recurrences = RecurrenceField()
    created_by = related.ForeignKey('base.User', null=True, on_delete=deletion.PROTECT, related_name="+")
    clan = related.ForeignKey('account.Clan', on_delete=deletion.PROTECT)
    who = related.ForeignKey('base.User', on_delete=deletion.PROTECT)
    what = CharField(max_length=255, default='', blank=True)
    how_much = MoneyField(max_digits=14, decimal_places=2, default=0)
    category = CharField(max_length=255, choices=ExpenseCategory.choices)
    enabled = BooleanField(default=True)
    next_occurrence = DateTimeField(null=True, default=None, blank=True)
    time = TimeField(default=time(9))
    time_zone = CharField(max_length=255, default=settings.TIME_ZONE, choices=[(tz, tz) for tz in sorted(zoneinfo.available_timezones())])
    