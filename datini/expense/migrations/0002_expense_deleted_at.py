# Generated by Django 4.2.1 on 2023-08-26 13:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('expense', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='expense',
            name='deleted_at',
            field=models.DateTimeField(default=None, null=True),
        ),
    ]
