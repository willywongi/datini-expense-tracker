from django.urls import path

from .views.expense import create_expense, all_expenses, home, update_expense, manifest, cancel_expense, restore_expense
from .views.recurring_expense import list_items, create_recurring_expense, update_recurring_expense

app_name = "expense"
urlpatterns = [
    path("", home, name="home"),
    path("create/", create_expense, name="create"),
    path("all/", all_expenses, name="all"),
    path("expense.webmanifest", manifest, name="manifest"),
    path("recurring/<pk>/update", update_recurring_expense, name="recurring_update"),
    path("<pk>/update", update_expense, name="update"),
    path("<pk>/cancel", cancel_expense, name="cancel"),
    path("<pk>/restore", restore_expense, name="restore"),
    
    path("recurring/", list_items, name="recurring_list"),
    path("recurring/create/", create_recurring_expense, name="recurring_create"),
    
]
