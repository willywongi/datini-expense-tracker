expense-when = quando
expense-who = chi
expense-what = cosa
expense-how_much = quanto
expense-category = categoria
expense-created-by = creato da
expense-is-deleted = Annullata

expense-category-household = Casa
expense-category-groceries = Spesa
expense-category-entertainment = Intrattenimento
expense-category-utilities = Utilities
expense-category-pets = Animali
expense-category-finance = Finanza
expense-category-kids = Figli

expense-create-title = Nuova
expense-create = Registra nuova spesa
expense-create-action = Registra
expense-back = Torna
expense-created = Spesa registrata

expense-all-time = Tutte le spese di sempre
expense-latest = Ultime spese ({$count})
expense-see-all = Vedi tutte
expense-what-na = N/D

expense-update = Aggiorna spesa
expense-update-action = Aggiorna
expense-update-delete = Annulla spesa
expense-update-restore = Ripristina spesa
expense-deleted = Spesa annullata
expense-restored = Spesa ripristinata

expense-nav-home = Spese
expense-nav-recurring = Ricorrenti
expense-nav-search = Cerca
expense-nav-create = Nuova

expense-recurrences = Ricorrenze
expense-recurrences-time-title = Orario della occorrenza
expense-recurrences-time = Orario
expense-recurrences-time-help = Puoi personalizzare l'orario in cui vuoi che Datini aggiunga la tua spesa.
      Se non diversamente specificato verrà usato "{ $recurrence_time }" nella tua ora locale.
expense-recurrences-time-zone = Fuso orario