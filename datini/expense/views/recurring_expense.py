import zoneinfo
import datetime
from urllib.parse import urlencode

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.forms import ModelChoiceField, RadioSelect, modelform_factory, ModelForm, HiddenInput, CharField, \
    ChoiceField
from django.forms import DateTimeInput
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone

from base.views import clan_required
from expense.methods import get_next_occurrence, create_current_expenses
from expense.models import RecurringExpense
from expense.fluent import bundle

_ = bundle.format
User = get_user_model()
RECURRING_EXPENSE_PARAMS_KEY = 'recurring-expense-params'



class HTMLTimeInput(DateTimeInput):
    input_type = "time"
    
    def format_value(self, value):
        return "{:%H:%M}".format(value) if value else ""


class RecurringExpenseForm(ModelForm):
    next = CharField(widget=HiddenInput)


@login_required
@clan_required
def list_items(request):
    queryset = RecurringExpense.objects.filter(clan=request.user.clan).order_by("-created_by")
    list_params = {}
    
    search_text = request.GET.get('search')
    if search_text:
        queryset = queryset.filter(what__icontains=search_text)
        list_params['search'] = search_text
    
    
    context = {
        'item_list': [{
            "pk": item.pk,
            "who": item.who,
            "what": item.what,
            "how_much": item.how_much,
            "get_category_display": item.get_category_display(),
            "is_yours": item.who == request.user,
            "next_occurrence": item.next_occurrence,
            "time_zone": zoneinfo.ZoneInfo(item.time_zone)
        } for item in queryset]
    }
    request.session[RECURRING_EXPENSE_PARAMS_KEY] = list_params
    return render(request, "recurring_expense/list.html", context)


@login_required
@clan_required
def create_recurring_expense(request):
    form_class = get_recurring_expense_form(request.user)
    next = request.GET.get('next', reverse("expense:recurring_list"))
    if request.method == "POST":
        form = form_class(request.POST)
        if form.is_valid():
            rexpense = form.save(commit=False)
            rexpense.clan = request.user.clan
            rexpense.next_occurrence = get_next_occurrence(rexpense.recurrences,
                                                           time=rexpense.time,
                                                           time_zone=rexpense.time_zone)
            rexpense.save()
            messages.add_message(request, messages.SUCCESS, _("expense-created"))
            return redirect(form.cleaned_data['next'])
    else:
        form = form_class(initial={'next': next,
                                   'who': request.user,
                                   'how_much': (None, "EUR")})
    
    context = {
        'form': form,
        'next': next,
        #'default_recurrence_time': RecurringExpense._meta.get_field('time').default
        'default_recurrence_time': f"{RecurringExpense._meta.get_field('time').default:%H:%M}"
    }
    return render(request, "recurring_expense/create.html", context)


def get_recurring_expense_form(user):
    form_fields = ("next", "who", "what", "how_much", "category",
                   "recurrences", "time", "time_zone")
    form_labels = {
        "who": _("expense-who").title(),
        "what": _("expense-what").title(),
        "how_much": _("expense-how_much").title(),
        "category": _("expense-category").title(),
        "time": _("expense-recurrences-time"),
        "recurrences": _("expense-recurrences").title(),
        "time_zone": _("expense-recurrences-time-zone").title()
    }
    form_widgets = {
        'category': RadioSelect,
        'time': HTMLTimeInput
    }
    
    def formfield_for_dbfield(db_field, **kwargs):
        if db_field.name == "who":
            return ModelChoiceField(queryset=User.objects.filter(clan=user.clan, is_active=True),
                                    label=_("expense-who"))
        return db_field.formfield(**kwargs)
    
    form_class = modelform_factory(RecurringExpense,
                                   RecurringExpenseForm,
                                   fields=form_fields,
                                   labels=form_labels,
                                   formfield_callback=formfield_for_dbfield,
                                   widgets=form_widgets)
    return form_class


@login_required
@clan_required
def update_recurring_expense(request, pk):
    recurring_expense = get_object_or_404(RecurringExpense.objects, pk=pk)
    form_class = get_recurring_expense_form(request.user)
    next = request.GET.get('next', reverse("expense:recurring_list"))
    list_params = request.session.get(RECURRING_EXPENSE_PARAMS_KEY)
    if list_params:
        next = f"{next}?{urlencode(list_params)}"
    
    if request.method == "POST":
        form = form_class(request.POST, instance=recurring_expense)
        if form.is_valid():
            rexpense = form.save(commit=False)
            rexpense.next_occurrence = get_next_occurrence(rexpense.recurrences,
                                                           time=rexpense.time,
                                                           time_zone=rexpense.time_zone)
            rexpense.save()
            messages.add_message(request, messages.SUCCESS, _("expense-created"))
            return redirect(form.cleaned_data['next'])
    else:
        form = form_class(initial={'next': next}, instance=recurring_expense)
    context = {
        'form': form,
        'next': next,
        'pk': pk,
    }
    return render(request, "recurring_expense/update.html", context)


# This should be called only internally
def post_current_expenses(request):
    create_current_expenses()
    return "{}"