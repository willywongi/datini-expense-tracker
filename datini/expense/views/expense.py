from urllib.parse import urlencode

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import Sum
from django.forms import DateTimeInput, ModelForm, CharField, HiddenInput, Form, RadioSelect, ModelChoiceField, \
    modelform_factory
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse
from django.views.decorators.cache import cache_control
from django.views.decorators.http import require_http_methods

from account.models import Clan
from base.views import clan_required
from base.templatetags.paginator_controls import paginator_controls
from expense.fluent import bundle
from expense.models import Expense

_ = bundle.format
User = get_user_model()
EXPENSE_ALL_PARAMS_KEY = "expense-all-params"


def get_user_label(user):
    if user.first_name and user.last_name:
        return f"{user.first_name} {user.last_name[0]}."
    elif user.last_name:
        return f"{user.last_name}"
    elif user.first_name:
        return f"{user.first_name}"
    else:
        return "{}".format(user.email.split("@")[0])


class HTMLDateTimeInput(DateTimeInput):
    input_type = "datetime-local"
    
    def format_value(self, value):
        return "{:%Y-%m-%dT%H:%M}".format(value) if value else ""


class SearchForm(Form):
    q = CharField(min_length=3)


class UserChoiceField(ModelChoiceField):

    def label_from_instance(self, obj):
        return get_user_label(obj)


@login_required
@clan_required
def create_expense(request):
    form_class = get_expense_form(request.user)
    next = request.GET.get('next', reverse("expense:home"))
    if request.method == "POST":
        form = form_class(request.POST)
        if form.is_valid():
            expense = form.save(commit=False)
            expense.clan = request.user.clan
            expense.save()
            messages.add_message(request, messages.SUCCESS, _("expense-created"))
            return redirect(form.cleaned_data['next'])
    else:
        form = form_class(initial={'next': next,
                                   'who': request.user,
                                   'how_much': (None, "EUR")})
    
    context = {'form': form, 'next': next}
    return render(request, "expense/create.html", context)


def get_expense_form(user):
    class ExpenseForm(ModelForm):
        next = CharField(widget=HiddenInput)
        who = UserChoiceField(queryset=User.objects.filter(clan=user.clan, is_active=True),
                                    label=_("expense-who"))

    form_fields = ("next", "who", "what", "how_much", "category", "when")
    form_labels = {
        "who": _("expense-who").title(),
        "what": _("expense-what").title(),
        "how_much": _("expense-how_much").title(),
        "category": _("expense-category").title(),
        "when": _("expense-when").title(),
    }
    form_widgets = {
        'when': HTMLDateTimeInput,
        'category': RadioSelect
    }

    form_class = modelform_factory(Expense,
                                   ExpenseForm,
                                   fields=form_fields,
                                   labels=form_labels,
                                   widgets=form_widgets)
    return form_class


@login_required
@clan_required
def all_expenses(request):
    queryset = Expense.all_objects.filter(clan=request.user.clan).order_by("-when")
    paginate_by = 10
    list_params = {}
    
    search_text = request.GET.get('search')
    if search_text:
        queryset = queryset.filter(what__icontains=search_text)
        list_params['search'] = search_text

    paginator = Paginator(queryset, paginate_by)
    page_obj = paginator.get_page(request.GET.get('page'))
    if page_obj.number != 1:
        list_params['page'] = page_obj.number
        
    controls = paginator_controls(request, page_obj)

    context = {
        'expense_list': [{
            "pk": expense.pk,
            "who": expense.who,
            "what": expense.what,
            "how_much": expense.how_much,
            "get_category_display": expense.get_category_display(),
            "when": expense.when,
            "is_yours": expense.who == request.user,
            "is_canceled": expense.deleted_at is not None
        } for expense in page_obj.object_list],
        'paginator': paginator,
        'page_obj': page_obj,
        **controls
    }
    request.session[EXPENSE_ALL_PARAMS_KEY] = list_params
    return render(request, "expense/list.html", context)


@login_required
@clan_required
def home(request):
    clan = Clan.objects.get(user=request.user)
    users = User.objects.filter(clan=clan, is_active=True)
    split_by = users.count()
    queryset = Expense.all_objects.filter(clan=request.user.clan).order_by("-when")

    expenses = Expense.objects.filter(clan=clan)
    totals = {item['who']: item['total'] for item in expenses.values("who").annotate(total=Sum('how_much'))}
    grand_total = Expense.objects.filter(clan=request.user.clan).aggregate(total=Sum('how_much'))['total']
    members = sorted([{
        'title': get_user_label(user),
        'is_self': user == request.user,
        'total': totals.get(user.pk, 0.0),
        'difference': grand_total / split_by - totals[user.pk] if user.pk in totals else None
    } for user in users], key=lambda item: not item['is_self'])

    context = {
        'clan': clan,
        'members': members,
        'expense_list': [{
            "pk": expense.pk,
            "who": expense.who,
            "what": expense.what,
            "how_much": expense.how_much,
            "get_category_display": expense.get_category_display(),
            "when": expense.when,
            "is_yours": expense.who == request.user,
            "is_canceled": expense.deleted_at is not None
        } for expense in queryset[:15]]
    }
    return render(request, "expense/home.html", context=context)


@login_required
@clan_required
def update_expense(request, pk):
    expense = get_object_or_404(Expense.all_objects, pk=pk)
    form_class = get_expense_form(request.user)
    next_url = request.GET.get('next', reverse("expense:home"))
    list_params = request.session.get(EXPENSE_ALL_PARAMS_KEY)
    if list_params:
        next_url = f"{next_url}?{urlencode(list_params)}"

    if request.method == "POST":
        form = form_class(request.POST, instance=expense)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, _("expense-created"))
            return redirect(form.cleaned_data['next'])
    else:
        form = form_class(initial={'next': next_url}, instance=expense)
    
    context = {'form': form, 'next': next_url, 'pk': pk, 'is_deleted': expense.deleted_at is not None}
    return render(request, "expense/update.html", context)


@cache_control(max_age=86_400)
def manifest(request):
    context = {
        'start_url': request.build_absolute_uri(reverse("expense:home")),
        'shortcut_create_name': bundle.format('expense-create')
    }
    return render(request, "expense/expense.webmanifest", context=context, content_type="application/manifest+json")


@require_http_methods(["POST"])
@login_required
@clan_required
def cancel_expense(request, pk):
    expense = get_object_or_404(Expense, pk=pk)
    next_url = request.POST.get('next', reverse("expense:home"))
    expense.soft_delete()
    messages.add_message(request, messages.SUCCESS, _("expense-deleted"))
    return redirect(next_url)


@require_http_methods(["POST"])
@login_required
@clan_required
def restore_expense(request, pk):
    expense = get_object_or_404(Expense.all_objects, pk=pk)
    next_url = request.POST.get('next', reverse("expense:home"))
    expense.restore()
    messages.add_message(request, messages.SUCCESS, _("expense-restored"))
    return redirect(next_url)
