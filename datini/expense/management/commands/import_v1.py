import json
import logging
import pathlib
from datetime import datetime

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from account.models import Clan
from expense.models import Expense

logger = logging.getLogger('datini.base')


class Command(BaseCommand):
    help = "Import the expenses from another instance"
    
    def add_arguments(self, parser):
        parser.add_argument("-i", "--input-file", type=pathlib.Path)
    
    def handle(self, *args, **options):
        User = get_user_model()
        with open(options['input_file']) as handler:
            source_set = json.load(handler)
        
        user_map = {}
        user_unknown = set()
        for d in source_set:
            source_who = d['fields']['who']
            if source_who not in user_unknown:
                try:
                    user_map[source_who] = User.objects.get(pk=source_who)
                except (User.DoesNotExist, ValueError):
                    user_unknown.add(source_who)
        
        self.stdout.write(f"Input file {options['input_file']} read.")
        self.stdout.write(f"Please enter a valid email for the following unknown users: ")
        for source_who in user_unknown:
            dest_email = input(f"{source_who} >>> ")
            user_map[source_who] = User.objects.get(email=dest_email)
            
        clan_map = {}
        clan_unknown = set()
        for d in source_set:
            source_clan = d['fields']['clan']
            if source_clan not in clan_unknown:
                try:
                    clan_map[source_clan] = Clan.objects.get(pk=source_clan)
                except (Clan.DoesNotExist, ValueError):
                    clan_unknown.add(source_clan)
        
        self.stdout.write(f"Please enter a valid clan code for the following unknown clans: ")
        for source_clan in clan_unknown:
            dest_code = input(f"{source_clan} >>> ")
            clan_map[source_clan] = Clan.objects.get(code=dest_code)
            
        for d in source_set:
            source_who = d['fields']['who']
            source_clan = d['fields']['clan']

            Expense.objects.create(who=user_map[source_who],
                                   clan=clan_map[source_clan],
                                   what=d['fields']['what'],
                                   when=datetime.fromisoformat(d['fields']['when'][:-1] + "+00:00"),
                                   category=d['fields']['category'],
                                   created_by=user_map[d['fields']['created_by']] if d['fields']['created_by'] else user_map[source_who],
                                   how_much=(d['fields']['how_much'], d['fields']['how_much_currency']))
        self.stdout.write(f"Successfully imported {len(source_set)} expenses")
