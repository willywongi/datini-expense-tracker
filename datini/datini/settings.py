import os
from collections import defaultdict
from pathlib import Path

from django.utils.log import DEFAULT_LOGGING

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
SECRET_KEY = os.environ['DJANGO_SECRET_KEY']
DEBUG = os.environ['DJANGO_DEBUG'] == "1"

# Network
ALLOWED_HOSTS = []
defined_hostname = os.environ.get('HOSTNAME')
if defined_hostname:
    ALLOWED_HOSTS.append(defined_hostname)

container_name = os.environ.get('BACKEND_CONTAINER_NAME')
if DEBUG and container_name:
    ALLOWED_HOSTS.append(container_name)

if DEBUG:
    ALLOWED_HOSTS.append(".ngrok-free.app")

# https://docs.djangoproject.com/en/5.0/ref/settings/#secure-proxy-ssl-header
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
USE_X_FORWARDED_HOST = True

# Application definition
INSTALLED_APPS = [
    'django_dramatiq',
    'base',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_bootstrap5',
    'django_ftl.apps.DjangoFtlConfig',
    'recurrence',
    'account',
    'expense',
    "invite",
    "takeout",
]

ROOT_URLCONF = 'datini.urls'
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages'
            ],
        },
    },
]

WSGI_APPLICATION = 'datini.wsgi.application'

# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': "/datini_db_data/db.sqlite",
    }
}
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# Internationalization
# https://docs.djangoproject.com/en/5.0/topics/i18n/
LANGUAGE_CODE = 'it-it'
TIME_ZONE = 'Europe/Rome'
USE_I18N = True
USE_L10N = True
USE_TZ = True
LANGUAGES = [('it', 'Italiano'),]

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/5.0/howto/static-files/
STATICFILES_DIRS = [
    BASE_DIR / "static",
]
STATIC_URL = f"{os.environ['STATIC_PATH']}/"
STATIC_ROOT = f"/datini-{os.environ['STATIC_PATH']}"
MEDIA_URL = f"{os.environ['MEDIA_PATH']}/"
MEDIA_ROOT = f"/datini-{os.environ['MEDIA_PATH']}"

# Authentication
AUTH_USER_MODEL = 'base.User'
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        "OPTIONS": {
            "min_length": 12,
        },
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = "/account/login"
LOGOUT_REDIRECT_URL = '/'

# Logging configuration
# https://docs.djangoproject.com/en/5.0/topics/logging/#configuring-logging
log_root = Path("/") / os.environ['LOG_PATH']
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': DEFAULT_LOGGING['filters'],
    "formatters": {
        "standard": {
            "format": '%(asctime)s %(name)s %(levelname)s %(message)s'
        },
        **DEFAULT_LOGGING['formatters']
    },
    'handlers': {
        'datini': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'filename': log_root / 'datini.log',
            'formatter': 'standard',
            "class": "logging.handlers.RotatingFileHandler",
            "maxBytes": 1048576,  # 1MB
            "backupCount": 5
        },
        'django': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'filename': log_root / 'django.log',
            'formatter': 'standard',
            "class": "logging.handlers.RotatingFileHandler",
            "maxBytes": 1048576,  # 1MB
            "backupCount": 5
        },
        **DEFAULT_LOGGING['handlers']
    },
    'loggers': {
        'django': {
            'handlers': ['django', 'mail_admins'],
            'level': 'INFO'
        },
        'datini': {
            'handlers': ['datini'],
            'level': 'DEBUG',
        },
        'django.server': DEFAULT_LOGGING['loggers']['django.server']
    },
}

# Email send configuration
# https://docs.djangoproject.com/en/5.0/topics/email/#smtp-backend
EMAIL_HOST = os.environ.get('EMAIL_HOST')
EMAIL_PORT = os.environ.get('EMAIL_PORT')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
# This custom backend allows composing email messages in MJML
EMAIL_BACKEND = "mjml_email_backend.MJMLEmailBackend"
# Default email address for automated correspondence from the site manager(s).
# This address is used in the `From` header of outgoing emails
DEFAULT_FROM_EMAIL = f"no-reply@{defined_hostname or 'example.com'}"

# Admins email
ADMINS = []
admin_names = os.environ.get('ADMIN_NAMES')
admin_email_addresses = os.environ.get('ADMIN_EMAIL_ADDRESSES')
if admin_names and admin_email_addresses:
    ADMINS = [(name.strip(), addr.strip()) for name, addr in zip(admin_names.split(","), admin_email_addresses.split(","))]
# Subject-line prefix for email messages sent to ADMINS and MANAGERS
EMAIL_SUBJECT_PREFIX = "Datini "
# The email address that error messages come from, such as those sent to ADMINS and MANAGERS.
SERVER_EMAIL = f"webmaster@{defined_hostname or 'example.com'}"

BOOTSTRAP5 = {
    'set_placeholder': False
}

DEFAULT_CURRENCY = "EUR"

# Dramatiq
DRAMATIQ_BROKER = {
    "BROKER": "dramatiq.brokers.redis.RedisBroker",
    "OPTIONS": {
        "host": os.environ['MESSAGE_BROKER_CONTAINER_NAME'],
    },
    "MIDDLEWARE": [
        "dramatiq.middleware.Prometheus",
        "dramatiq.middleware.AgeLimit",
        "dramatiq.middleware.TimeLimit",
        "dramatiq.middleware.Callbacks",
        "dramatiq.middleware.Retries",
        "dramatiq.middleware.CurrentMessage",
        "django_dramatiq.middleware.DbConnectionsMiddleware",
    ]
}
#DRAMATIQ_ENCODER = "dramatiq.encoder.PickleEncoder"
DRAMATIQ_AUTODISCOVER_MODULES = ["tasks", "methods"]
