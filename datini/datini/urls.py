from django.urls import path, include, re_path
from django.views.i18n import JavaScriptCatalog

from base.views import home, font
from expense.views.expense import manifest

urlpatterns = [
    path('', home, name='home'),
    path('font.css', font, name='font'),
    path('account/', include("account.urls")),
    path('invite/', include("invite.urls")),
    path("manifest.json", manifest, name="manifest"),
    path("expense/", include("expense.urls")),
    path("takeout/", include("takeout.urls")),
]

# jsi18n can be anything you like here
urlpatterns += [
    path('jsi18n/', JavaScriptCatalog.as_view(packages=['recurrence']), name="javascript-catalog"),
]