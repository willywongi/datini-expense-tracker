from django.contrib import messages
from django.contrib.auth.views import PasswordResetDoneView, PasswordResetConfirmView
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import reverse_lazy, reverse
from django.views.decorators.http import require_http_methods

from . import methods
from .forms import InviteForm, SendInviteForm, InviteSetPasswordForm
from base.views import clan_required
from account.fluent import bundle


@clan_required
def invite_create(request):
    if request.method == "POST":
        form = InviteForm(request.POST)
        if form.is_valid():
            methods.create_invited_user(form.cleaned_data["email"], clan=request.user.clan)
            form.save(request=request,
                      extra_email_context={
                          "clan": request.user.clan,
                          "sender": f"{request.user}"
                      })
            email = form.cleaned_data['email']
            next_url = form.cleaned_data.get("next_url") or reverse("account:profile")
            messages.add_message(request, messages.SUCCESS, bundle.format("account-invite-success", args=(email,)))
            return HttpResponseRedirect(next_url)
    else:
        form = InviteForm()

    context = {
        'form': form
    }
    return TemplateResponse(request, "account/invite-create.html", context=context)


@clan_required
@require_http_methods(["POST"])
def invite_resend(request):
    form = SendInviteForm(request.POST)
    if form.is_valid():
        extra_email_context = {
            "clan": request.user.clan,
            "sender": f"{request.user}"
        }
        form.save(request=request,
                  extra_email_context=extra_email_context)

        email = form.cleaned_data['email']
        next_url = form.cleaned_data.get("next_url") or reverse("account:profile")
        messages.add_message(request, messages.SUCCESS, bundle.format("account-invite-success", args=(email,)))
        return HttpResponseRedirect(next_url)
    else:
        context = {
            'form': form
        }
        return TemplateResponse(request, "account/invite-create.html", context=context)


@clan_required
@require_http_methods(["POST"])
def invite_cancel(request):
    email = request.POST['email']
    next_url = request.POST.get("next_url") or reverse("account:profile")
    methods.delete_invited_user(email)
    messages.add_message(request, messages.SUCCESS, bundle.format("account-invite-success", args=(email,)))
    return HttpResponseRedirect(next_url)


# Presents a form for entering a new password.
set_password = PasswordResetConfirmView.as_view(template_name="invite/set_password.html",
                                                form_class=InviteSetPasswordForm,
                                                success_url=reverse_lazy("invite:set_password_done"))

# Presents a view which informs the user that the password has been successfully changed.
password_reset_confirm_done = PasswordResetDoneView.as_view(template_name="invite/set_password_done.html")
