from django.contrib.auth import get_user_model

User = get_user_model()


def create_invited_user(email, clan):
    user = User(email=email, clan=clan, is_active=False)
    user.set_unusable_password()
    user.save()
    return user


def delete_invited_user(email):
    email_field_name = User.get_email_field_name()
    user_selector = {
        f"{email_field_name}__iexact": email,
        "is_active": False
    }
    try:
        user = User.objects.get(**user_selector)
    except User.DoesNotExist:
        pass
    else:
        user.delete()
