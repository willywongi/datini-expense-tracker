from django.urls import path

from . import views

app_name = "invite"
urlpatterns = [
    path("create", view=views.invite_create, name="create"),
    path("resend", view=views.invite_resend, name="resend"),
    path("cancel", view=views.invite_cancel, name="cancel"),
    path("set_password/<uidb64>/<token>/",
         view=views.set_password, name='set_password_confirm', ),
    path("set_password/done/", view=views.password_reset_confirm_done, name='set_password_done'),
]