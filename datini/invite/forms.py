from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.forms import ValidationError
from django.forms.fields import EmailField, CharField
from django.forms.widgets import EmailInput
from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm

from account.fluent import bundle

_ = bundle.format_lazy
User = get_user_model()


class AlreadyInvited(Exception):
    pass


class AlreadyAccepted(Exception):
    pass


def validate_invitation(email):
    email_field_name = User.get_email_field_name()
    qs = User.objects.filter(
        **{
            f"{email_field_name}__iexact": email,
        }
    )

    if qs.filter(is_active=False):
        raise AlreadyInvited
    elif qs.filter(is_active=True):
        raise AlreadyAccepted
    else:
        return True


class InviteForm(PasswordResetForm):
    next_url = CharField(required=False)
    email = EmailField(
        label=_("account-invite-email"),
        required=True,
        widget=EmailInput(attrs={"autocomplete": "email"}),
        initial="",
    )

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            validate_invitation(email)
        except AlreadyInvited:
            raise ValidationError(_("account-invite-already-invited"))
        except AlreadyAccepted:
            raise ValidationError(_("account-invite-already-accepted"))
        return email

    def get_users(self, email):
        email_field_name = User.get_email_field_name()
        return User.objects.filter(**{f"{email_field_name}__iexact": email})

    def save(
        self,
        domain_override=None,
        subject_template_name="invite/invite_email_subject.txt",
        email_template_name="invite/invite_email.txt",
        use_https=False,
        token_generator=default_token_generator,
        from_email=None,
        request=None,
        html_email_template_name="invite/invite_email.mjml",
        extra_email_context=None,
    ):
        return super().save(domain_override=domain_override,
                            subject_template_name=subject_template_name,
                            email_template_name=email_template_name,
                            use_https=use_https,
                            token_generator=token_generator,
                            from_email=from_email,
                            request=request,
                            html_email_template_name=html_email_template_name,
                            extra_email_context=extra_email_context)


class SendInviteForm(InviteForm):

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            validate_invitation(email)
        except AlreadyInvited:
            pass
        except AlreadyAccepted:
            raise ValidationError(_("account-invite-already-accepted"))
        return email


class InviteSetPasswordForm(SetPasswordForm):
    def save(self, commit=True):
        self.user = super().save(commit=False)
        self.user.is_active = True
        self.user.save()
        return self.user
