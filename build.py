#!/usr/bin/python3
import re
import subprocess
from argparse import ArgumentParser


CONFIG = [{
    'image': 'willywongi/datini',
    'context': "datini",
    'target': "backend"
}, {
    'image': 'willywongi/datini-frontend',
    'context': "frontend"
}, {
    'image': 'willywongi/datini-worker',
    'context': "datini",
    'target': 'worker'
}, {
    'image': 'willywongi/datini-scheduler',
    'context': "scheduler"
}]
PLATFORMS = [
    "linux/amd64",
]

MATCH_TAG = re.compile(r"v\d+\.\d+")
MAJOR = 1


def git(*subprocess_args):
    cp = subprocess.run(["git", *subprocess_args], capture_output=True)
    return cp.stdout.decode().strip()


def get_next_version(major):
    cp = git("tag")
    if cp:
        prev_versions = [int(r.replace(f"v{major}.", "")) for r in cp.split("\n") if MATCH_TAG.match(r)]
        next_minor = max(prev_versions) + 1
    else:
        next_minor = 0
    return f"v{major}.{next_minor}"


def build(version):
    last_commit = git("log", "-1", "--oneline")
    commit_hash = last_commit[:7]
    print(git("tag", version, commit_hash))
    print(f"Repo tagged with {version}")
    for platform in PLATFORMS:
        for config in CONFIG:
            tags_args = []
            for tag in (version, 'latest'):
                tags_args.extend(["--tag", f"{config['image']}:{tag}"])

            target = config.get('target')
            target_args = []
            if target:
                target_args.extend(["--target", target])
            print(["docker", "buildx", "build", "--platform", platform, *tags_args, *target_args, "--push", config["context"]])
            subprocess.run(["docker", "buildx", "build", "--platform", platform, *tags_args, *target_args, "--push", config["context"]])


parser = ArgumentParser()
parser.add_argument('--minor', help="The minor version number", type=int)
parser.add_argument('--major', help="The major version number", default=MAJOR, type=int)
#parser.add_argument('--no-push', help="Skip push to repository", default=False, type=bool)


if __name__ == "__main__":
    branch = git("branch", "--show-current")
    if branch != "production":
        raise Exception("Not a production branch, I refuse to build.")
    args = parser.parse_args()
    if args.minor:
        version = f"v{args.major}.{args.minor}"
    else:
        version = get_next_version(args.major)
    build(version)

