# Datini expense tracker

This is a web app to track expenses. For individuals, teams, families, guilds, clans.

## Principles
### Less features
It has just the minimum features to work. I will resist to any request to add features.

### Less tech
It's deployed using what I know to make it work with the least amount of technology involved.

### Less data
Only the bare minimum information is collected: no usage statistics, no complicated dashboards.

## How it works
The main item is a Django full-stack app. The database is a single sqlite file. The only suggested installation mode is
through `docker` – a `compose.yaml` example file, suitable for "production", is provided.

Services involved:
- `backend`: this is the main Django full-stack app, with an embedded `sqlite` database
- `frontend`: a Caddy server for static files and reverse proxy.
- `worker` the same codebase for `backend`, but it's running the `dramatiq` worker spanner.
- `message_broker`: a Redis instance.
- `scheduler`: a simple image with `supercronic`, an alternative to `cron` fit for containers. It runs an hourly python 
script adding a task for creating expenses from recurring definitions.

## Installation
### A virtual private server
Use the smallest possible VPS your provider is offering. For example (March 2024), you can rent a compute instance from Hetzner for 
~5€/month (see the CX11 plan). If unsure, install Debian. Take note of the IP address (v4 and IPv6, if available).

If you need some ground on how to start self-hosting some services, I found
[this guide](https://becomesovran.com/blog/server-setup-basics.html) helpful.

[Install Docker](https://docs.docker.com/engine/install/), and remember to follow the [post-installation notes](https://docs.docker.com/engine/install/linux-postinstall/). 

Make sure that the ports 80 and 443 are open.

### A domain name
Buy a domain name and set up the DNS to point to your VPS's IP address. You are going to set
new `A` and/or `AAAA` (for IPv6) records.

### A transactional email service
Datini sends really few emails. At the moment of writing (aug/24), only for invitations. So you'll need
to subscribe to a _transactional email servce_ (TEM). Some of the free options I know of are:
- [Mailgun](https://www.mailgun.com/): US based. 100 emails/day. 
- [Scaleway TEM](https://www.scaleway.com/en/transactional-email-tem/): EU based. 300 emails/month.
- [Mailslurp](https://www.mailslurp.com/): EU based. 100 emails/month
- [SMTP2GO](https://www.smtp2go.com/): AU based. 200 emails/day (1000 /month).

Most, if not all, TEM providers require a credit card, even for the free plan.


### Folder and files
Create a folder in your VPS. You'll gather there some files from this repository.

Copy the files in the `sample_configs` folder in this repository and edit those files. 
When a line starts with a comment, that configuration is optional; otherwise is mandatory. Sensible defaults are provided.

```
# Mandatory:
DJANGO_SECRET_KEY=

# Optional:
#ADMIN_NAMES=

# Sensible default
BACKEND_CONTAINER_NAME=datini-backend
```

If you're already using [Traefik](https://traefik.io) you can use `compose.traefik.yaml`. Make sure to:
- have a `RedirectScheme` middleware set up (in order to catch `http` requests and upgrade them to `https`);
- Datini and Traefik must be in the same network: fix the network name in the compose file. 

If you're using other reverse proxies you should change the compose file accordingly.

Otherwise, you can happily use the `compose.yaml` file: the `frontend` container is a [Caddy server](https://caddyserver.com)
that will kindly take care of managing TLS certificates for you.

At the end, you should have these 3 files:
- `.env`
- `compose.yaml`
- `service.env`

### First run
Set up the database:
```
docker compose run --rm worker migrate
```

Create the first user:
```
docker compose run --rm worker createsuperuser 
```

At last, start it up: 
```
docker compose up -d 
```

## Usage
### Login/signup
The first user must be created through the Django management commands (`createsuperuser` or `createuser`). Once you have an account, 
you can invite other users. The invited users are actual Django users set with no usable password and the attribute `is_active` set to `False`.
In the invite email message is a link to set a usable password. Once set, the user object is updated with `is_active=True`. 

### Pick your clan
If you're the first user of this setup, you can create a new clan. Otherwise, you should have a code to join a clan. 

### Add your first expense
At the bottom of the expense table, right side, you'll find a button to create an expense. Then, in the form you're 
presented, you should fill in the details:
- Who: by default, it's you. But you can add expenses for others in your clan.
- What: what's the expense is about?
- How much: the monetary value (eg.: 10)
- Category
- When: using your OS and browser controls, you should be able to pick "now" easily; otherwise, a date and time is required.

Click on Save button.

### Edit or cancel the expense
Clicking the title of the expense, you can update details of the expense. You can also cancel the expense. 

The expenses are "soft-deleted", so you'll always be able to un-cancel the expense.

## Development
To start a development environment, clone this repo first.

Copy `sample_configs/.env` and `sample_configs/service.env` in the root folder. Edit according to instructions in these
files.

Copy `docker-compose.dev.yaml` into a new file called `docker-compose.override.yaml`.

Set up the database:
```
docker compose run --rm worker migrate
```

Create the first user:
```
docker compose run --rm worker createsuperuser 
```

At last, start it up: 
```
docker compose up -d 
```

You can now visit http://localhost:8000.
