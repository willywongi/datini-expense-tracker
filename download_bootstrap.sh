curl -L --output bootstrap.zip https://github.com/twbs/bootstrap/archive/v5.3.2.zip
unzip bootstrap.zip
mv bootstrap-5.3.2/scss/ datini/static/bootstrap/
rm -rf bootstrap-5.3.2/
rm bootstrap.zip
