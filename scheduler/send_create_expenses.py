import os
import json
from uuid import uuid4

from redis import Redis

redis = Redis(host=os.environ['MESSAGE_BROKER_CONTAINER_NAME'])

# https://dramatiq.io/advanced.html#enqueueing-messages-from-other-languages
my_redis_message_id = str(uuid4())
message_id = str(uuid4())
message = {
  "queue_name": "default",
  "actor_name": "create_current_expenses",
  "args": [],
  "kwargs": {},
  "options": {
    "redis_message_id": my_redis_message_id
  },
  "message_id": message_id,
  "message_timestamp": 0
}
redis.hset("dramatiq:default.msgs", my_redis_message_id, json.dumps(message))
redis.rpush("dramatiq:default", my_redis_message_id)
